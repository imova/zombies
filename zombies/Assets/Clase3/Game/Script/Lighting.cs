﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour {

	public float LifeTime;

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable()
	{
		Invoke ("SetAct", LifeTime);
	}

	void SetAct()
	{
		gameObject.SetActive (false);
	}
	// Update is called once per frame
	void Update () {
		
	}
}
