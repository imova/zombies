﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartApp : MonoBehaviour {
    float _buttonDownPhaseStart, _doubleClickPhaseStart;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            _buttonDownPhaseStart = Time.time;
        }

        if (_doubleClickPhaseStart > -1 && (Time.time - _doubleClickPhaseStart) > 0.2f)
        {
            Debug.Log("single click");
            _doubleClickPhaseStart = -1;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (Time.time - _buttonDownPhaseStart > 1.0f)
            {
                Debug.Log("long click");
                Application.LoadLevel(0);
                _doubleClickPhaseStart = -1;
            }
            else
            {
                if (Time.time - _doubleClickPhaseStart < 0.2f)
                {
                    Debug.Log("double click");
                    _doubleClickPhaseStart = -1;
                }
                else
                {
                    _doubleClickPhaseStart = Time.time;
                }
            }
        }
    }
}
