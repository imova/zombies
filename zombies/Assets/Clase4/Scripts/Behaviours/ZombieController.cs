﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour {

    // Use this for initialization


    [SerializeField] bool forward;
    [SerializeField] float Health = 100;
    [SerializeField] int myDamage = 10;

    public ParticleSystem bloodEmitter;
    public float Speed = 0;


    public float SpeedFactor = 1;
    Animator animatorController;
    AnimatorStateInfo animatorStateInfo;
    NavMeshAgent agent;

    MainPlayerController mainPlayerController;

    enum ZombieState {none, idle, walk, attack, die}

    ZombieState zombieState = ZombieState.none;
    ZombieState prevZombieState = ZombieState.none;


    // Use this for initialization
    void Start()
    {
        animatorController = GetComponent<Animator>();
        bloodEmitter = GetComponentInChildren<ParticleSystem>();
        agent = GetComponent<NavMeshAgent>();
        mainPlayerController = FindObjectOfType<MainPlayerController>();
        

        SwitchZombieState(ZombieState.walk);

        
        agent.SetDestination(mainPlayerController.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        animatorStateInfo = animatorController.GetCurrentAnimatorStateInfo(0);
        if (animatorStateInfo.IsName("Zombie_Walk_01")) {

            if (mainPlayerController.Healt == 0)
            {
                SwitchZombieState(ZombieState.idle);
                agent.speed = 0;
            }
            else
            {
                Speed = animatorController.GetFloat("Speed");
                SpeedFactor = animatorController.GetFloat("SpeedMultiply");
                float agentRemain = agent.remainingDistance - agent.stoppingDistance;

                if (agentRemain < 0)
                {
                    agent.speed = 0;
                    SwitchZombieState(ZombieState.attack);
                }
                else
                {
                    agent.speed = Speed * SpeedFactor;
                }
            }
        }
    }


    void SwitchZombieState(ZombieState state) {
        animatorController.speed = 1;
        if (state != prevZombieState)
            switch (state) {
                case ZombieState.walk:
                    animatorController.SetTrigger("walk");
                    if (gameObject.name == "Zombie1(Clone)" || gameObject.name == "Zombie1")
                    {
                        animatorController.SetFloat("SpeedMultiply",2);
                    }
                    else
                    {
                        animatorController.SetFloat("SpeedMultiply",1);
                    }

                    break;
                case ZombieState.attack:
                    animatorController.SetTrigger("attack");
                    InvokeRepeating("AttackPlayer",1,1);
                    break;
                case ZombieState.die:
                    animatorController.SetTrigger("death");
                    mainPlayerController.KillZombie();
                    GetComponent<Collider>().enabled = false;
                    GetComponent<AudioSource>().volume = 0;
                    GameObject.Destroy(this.gameObject, 2);
                    break;
                case ZombieState.idle:
                    animatorController.SetTrigger("idle");
                    GetComponent<AudioSource>().volume = 0;
                    break;

                default:
                    break;

            }
        prevZombieState = state;
    }

    public void OnShoot() {
        bloodEmitter.Play();
        
    }

    public void OnTriggerEnter(Collider other)
    {
        {
            if (other.gameObject.tag == "bullet")
            {
                bloodEmitter.transform.position = other.ClosestPoint(other.transform.position);
                ReceiveDamage(10);
                bloodEmitter.Play();
            }
        }
    }
    public void ReceiveDamage(float damage) {
        Health -= damage;
        if (Health <= 0) {
            SwitchZombieState(ZombieState.die);
        }

    }

    void AttackPlayer() {
        if (mainPlayerController.TakeDamage(myDamage)) {
            Debug.Log("Died");
            if (zombieState != ZombieState.idle)
            {
                SwitchZombieState(ZombieState.idle);
            }
        }
    }


}
