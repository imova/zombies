﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public MainPlayerController playerController;       // Reference to the player's heatlh.
    public GameObject[] enemies;                // The enemy prefab to be spawned.
    public GameObject enemy;
    public float spawnTime = 3f;            // How long between each spawn.
    public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.


    void Start()
    {
        // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn()
    {
        // If the player has no health left...
        if (playerController.Healt <= 0f)
        {
            // ... exit the function.
            return;
        }

        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        float rangeZ = Random.Range(-5, 5);
        float rangeX = Random.Range(-5, 5);

        Vector3 offset = new Vector3(rangeX, 0, rangeZ);

        enemy = enemies[Random.Range(0, enemies.Length)];
        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(enemy, spawnPoints[spawnPointIndex].position+offset, spawnPoints[spawnPointIndex].rotation);
    }
}
