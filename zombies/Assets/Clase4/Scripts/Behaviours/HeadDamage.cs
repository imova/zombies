﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadDamage : MonoBehaviour {
    public ZombieController zombieScript;
	// Use this for initialization
	void Start () {
        zombieScript = GetComponentInParent<ZombieController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter(Collider other)
    {
        {
            if (other.gameObject.tag == "bullet")
            {
                zombieScript.bloodEmitter.transform.position = other.ClosestPoint(other.transform.position);
                zombieScript.bloodEmitter.Play();
                zombieScript.ReceiveDamage(100);
            }
        }
    }
}
