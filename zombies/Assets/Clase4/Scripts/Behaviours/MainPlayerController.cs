﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPlayerController : MonoBehaviour {
    public int Healt = 100;
    public int Kills = 0;

    public GameObject LoadLevelScreen;

    public Text KillsText;
    public Slider LifeSlider;

    bool imDied = false;
    Rigidbody rigidBody;

    AudioSource audioSource;
	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool TakeDamage(int damage) {
        if (Healt >0)
            Healt -= damage;
        if (Healt <= 0 && imDied == false) {
            audioSource.Play();
            imDied = true;
            StartCoroutine(Die());
        }
        LifeSlider.value = Healt / 100f;

        return Healt == 0;
    }

    public void KillZombie() {
        Kills++;
        KillsText.text = Kills.ToString();
        Debug.Log("Toma eso maldito Zombie!");
    }

    IEnumerator Die() {
        //transform.Rotate(new Vector3(-90, 0, 0));
        LoadLevelScreen.SetActive(true);
        for (int i = 0; i < 100; i++) { 
            yield return new WaitForSeconds(.01f);
            transform.Translate(0, -.01f, 0);
        }
    }
}
